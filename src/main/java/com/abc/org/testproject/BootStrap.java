package com.abc.org.testproject;

import org.apache.catalina.startup.Bootstrap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class BootStrap extends SpringBootServletInitializer {
	
	public static void main(String[] args)
	{
		SpringApplication app = new SpringApplication(BootStrap.class);
		app.run(args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
	{
		return application.sources(Bootstrap.class);
	}

}
